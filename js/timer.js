function showTextTime()
	{
		const currentDate = new Date();
		
		let getHours = currentDate.getHours();
		if (getHours<10) getHours = "0"+getHours;
		
		let getMinutes = currentDate.getMinutes();
		if (getMinutes<10) getMinutes = "0"+getMinutes;
		
		let getSeconds = currentDate.getSeconds();
		if (getSeconds<10) getSeconds = "0"+getSeconds;
		
		document.getElementById("watch").innerHTML = getHours+":"+ getMinutes+":"+ getSeconds;
		 
		 setTimeout("showTextTime()",1000);
	}
window.onload= showTextTime()
